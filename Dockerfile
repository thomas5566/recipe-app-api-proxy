FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="thomas19840826@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
# touch create empty file
# when run applictaion need to run M substitute
# which is entrypoint.sh to take out template file replace the
# evbironment variables
RUN touch /etc/nginx/conf.d/default.conf
# allow the nginx user running the entrypoint.sh to update this
# default.conf with the template file updated with variables we defined
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# switch back to the nginx user when build is finished the user is set to enginex
# This means that when you run the docker container from the build image it will
# run it as the last user switched to why you are building your image in your file
USER nginx

CMD ["/entrypoint.sh"]